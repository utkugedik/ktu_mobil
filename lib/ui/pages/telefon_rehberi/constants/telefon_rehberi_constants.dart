import 'package:ktu_mobil/ui/pages/telefon_rehberi/model/telefon_rehberi_model.dart';

class TelephoneDirectoryConstants {
  static final List<TelefonRehberiModel> list = [
    TelefonRehberiModel(
      name: 'Orman Fakültesi',
      telephoneNumber: '+90 462 377 2800',
    ),
    TelefonRehberiModel(
      name: 'Orman Fakültesi Orman Mühendisligi Bölümü',
      telephoneNumber: '+904623772805',
    ),
    TelefonRehberiModel(
      name: 'Orman Fakültesi Orman Endüstri Mühendisligi Bölümü',
      telephoneNumber: '0 (462) 377 3248',
    ),
    TelefonRehberiModel(
      name: 'Orman Fakültesi Peyza Mimarligi Bölümü',
      telephoneNumber: '0 (462) 377 4050',
    ),
    TelefonRehberiModel(
      name: 'Mühendislik Fakültesi',
      telephoneNumber: '+90 462 377 2701',
    ),
    TelefonRehberiModel(
      name: 'Mühendislik Fakültesi Bilgisayar Mühendisligi',
      telephoneNumber: '+90 462 377 3157',
    ),
    TelefonRehberiModel(
      name: 'idari ve Mali isler Daire Baskanligi',
      telephoneNumber: '+90 (462) 377 2217 - +90 (462) 325 3192',
    ),
    TelefonRehberiModel(
      name: 'Dis Hekimligi Fakültesi',
      telephoneNumber: '+90 462 377 4700',
    ),
    TelefonRehberiModel(
      name: 'Eczacilik Fakültesi',
      telephoneNumber: '+90 462 325 6762',
    ),
    TelefonRehberiModel(
      name: 'Edebiyat Fakültesi',
      telephoneNumber: '+90 462 325 55 97',
    ),
  ];
}
