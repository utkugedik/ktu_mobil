import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/ui/pages/telefon_rehberi/constants/telefon_rehberi_constants.dart';

import 'package:ktu_mobil/ui/shared/custom_appbar.dart';
import 'package:ktu_mobil/ui/shared/custom_divider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class TelefonRehberiPage extends HookConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: const CustomAppbar(title: 'ACİL NUMARALAR'),
      body: ListView.separated(
        itemCount: TelephoneDirectoryConstants.list.length,
        separatorBuilder: (_, __) => const CustomDivier(),
        itemBuilder: (context, index) {
          var list = TelephoneDirectoryConstants.list[index];
          return ListTile(
            onTap: () => launchUrl(
              Uri(
                scheme: 'tel',
                path: list.telephoneNumber,
              ),
            ),
            leading: Icon(
              MdiIcons.viewList,
              color: Colors.blue[800],
            ),
            title: Text(
              list.name,
              style: Theme.of(context).textTheme.headline6,
            ),
            subtitle: Text(
              list.telephoneNumber,
              style: Theme.of(context).textTheme.headline6,
            ),
            trailing: const Icon(
              MdiIcons.phoneInTalk,
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}
