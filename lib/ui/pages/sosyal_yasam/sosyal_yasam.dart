import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class SosyalYasamPage extends HookConsumerWidget {
  const SosyalYasamPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('SOSYAL YASAM'),
          bottom: const TabBar(
            isScrollable: true,
            tabs: [
              Tab(text: 'KONAKLAMA'),
              Tab(text: 'BESLENME SAGLIK'),
              Tab(text: 'SOSYAL-KULTUREL VE SPORTIF TESISLER'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: Html(data: """
           <div class="tab-pane active" id="sosyalyasam0">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-banner" style="padding:5px;">ÖĞRENCİ YURTLARI</h4><br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <p><strong>Koru Öğrenci Yurdu</strong></p>
            <p align="justify">Merkez Kanuni Kampüsü içinde yer alan KTÜ Öğrenci Yurdu,&nbsp;üniversitemize gelen
                öğrencilerimize hizmet vermektedir. Yurdumuz,&nbsp;toplamda 86 kişilik kapasitesi ile&nbsp;31 odaya
                sahiptir.</p>
            <p>Telefon : 0 (462) 377 3578<br></p>
        </div>
        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_a0913.jpg" class="img-polaroid" width="200" alt="Koru Yurdu"
                data-toggle="modal" data-target="#myModal1">
            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Koru Yurdu</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_70b0a.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <p><strong>Kredi ve Yurtlar Kurumu Yurtları</strong></p>
            <p align="justify">Üniversitemiz Merkez Kanuni Kampüsü'nde Kredi ve Yurtlar Kurumu tarafından yürütülen iki
                ayrı yurt kompleksimiz bulunmaktadır. Doğu Karadeniz Öğrenci Yurdu, sekiz (8) bloktan oluşmakta ve 1-3
                kişilik odalarda kız öğrencilerimize kalacak yer imkanı sağlanmaktadır. Diğer yurt kompleksi ise dört
                (4) ayrı bloktan oluşmakta ve erkek öğrencilerimize barınma imkanı sağlanmaktadır. Bunların dışında
                Trabzon şehir merkezinde, Akçaabat, Yomra, Sürmene, Of ve Maçka'da kız ve erkek öğrencilerimiz için
                yurtlar bulunmaktadır.</p>
            <p>
            </p>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Kredi ve Yurtlar Kurumu Trabzon Bölge
                Müdürlüğü</div>
            Telefon: 0 (462) 230 7340<br><br>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Trabzon Yurt Müdürlüğü</div>
            Telefon: 0 (462) 328 0982- 377 3161<br><br>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Doğu Karadeniz Yurt Müdürlüğü</div>
            Telefon: 0 (462) 328 0101- 328 0110
            <p></p>
        </div>
        <div class="col-md-3"><br>
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_f42fa.jpg" class="img-polaroid" width="200"
                alt="Doğu Karadeniz Yurdu Yurtları" data-toggle="modal" data-target="#myModal2">
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Doğu Karadeniz Yurtları</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_b284f.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div style="width:10px;height:15px;"></div>
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_7a962.jpg" class="img-polaroid" width="200"
                alt="Erkek Öğrenci Yurtları" data-toggle="modal" data-target="#myModal5">
            <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Erkek Öğrenci Yurtları</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_7f7b0.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <div class="row" style="margin-top:30px;">
        <div class="col-md-12">
            <h3 class="page-banner" style="padding:5px;">SOSYAL TESİSLER</h3><br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p><strong>KTÜ Turizm Eğitim ve Uygulama Merkezi İktisadi İşletmesi</strong></p>
            <p align="justify">Sosyal Tesisler İşletme Müdürlüğü, 13/12/2002 tarihinde yürürlüğe giren “Turizm Eğitim ve
                Uygulama Merkezi İşletme Yönergesi’’ ile&nbsp; çalışmalarına başlamış olup Sahil Tesisleri ve Koru
                Tesisleri olmak üzere iki farklı mekanda hizmet vermektedir.</p>
            <p>Web Sayfası: <a href="http://www.ktu.edu.tr/sosyaltesisler"
                    target="_blank">www.ktu.edu.tr/sosyaltesisler</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <p><strong>Sahil Tesisleri</strong></p>
            <p align="justify">
                Sahil Tesisleri, standart ve suit odalar, zarif ve işlevsel ayrıntılarla dekore edilmiştir. Toplamda 35
                oda ve 75 yatak kapasitesine sahiptir. Ücretsiz, güvenilir, geniş otoparkı imkânı mevcuttur. Kahvaltı
                salonumuz zengin içeriği ile farklı tat ve lezzetleri bir arada bulundurmaktadır. Ayrıca, isteğe göre
                ağırlamakta olduğumuz gruplar için öğlen ve akşam yemekleri de servis edilmektedir.
            </p>
            <p>
            </p>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Resepsiyon</div>
            Telefon: 0 (462) 377 4900
            <p></p>
            <p>
            </p>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Restaurant</div>
            Telefon: 0 (462) 377 4957<br>
            <p></p>
            <p>
                Web Sayfası: <a href="http://www.ktu.edu.tr/sahil" target="_blank">www.ktu.edu.tr/sahil</a><br>
            </p>
        </div>
        <div class="col-md-3"><br>

            <img src="http://www.ktu.edu.tr/dosyalar/ktu_1bef2.jpg" class="img-polaroid" width="200"
                alt="Sahil Tesisleri" data-toggle="modal" data-target="#myModal3">
            <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Sahil Tesisleri</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/sahil_20a3a.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <p><strong>Koru Tesisleri</strong></p>
            <p align="justify">
                Koru Tesisleri, merkez kampüsü içerisinde toplam 108 adet oda ve suiti ile siz değerli misafirlerimize
                hizmet vermektedir. Tesisimiz, havalimanı ve otobüs terminaline 5 dakika uzaklıkta olup trafikten uzak
                yeşil ve denizle iç içe bir konuma sahiptir.
            </p>
            <p>
                Telefon: 0 (462) 377 3547
            </p>
            <p>
                Web Sayfası: <a href="http://www.ktu.edu.tr/koru" target="_blank">www.ktu.edu.tr/koru</a><br>
            </p>
        </div>

        <div class="col-md-3"><br>
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_ec71c.jpg" class="img-polaroid" width="200"
                alt="Koru Tesisleri" data-toggle="modal" data-target="#myModal4">
            <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Koru Tesisleri</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/koru_7e518.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>
</div> 
"""),
            ),
            SingleChildScrollView(
              child: Html(data: """ 
             <div class="tab-pane  active" id="sosyalyasam1">

    <div class="row" >
        <div class="col-md-12">
            <h3 class="page-banner" style="padding:5px;">BESLENME</h3><br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p align="justify">
                Üniversitemizin çeşitli kampüslerinde öğrenci yemekhaneleri bulunmaktadır. Özel sektör tarafından
                işletilen yemekhanede öğrenciler maliyetin yarısını ödeyerek öğle yemeklerinden yararlanabilmektedirler.
                Kredi ve Yurtlar Kurumu (KYK) yurtlarında kalan öğrencilerin sabah ve akşam yemek maliyetleri ise KYK
                katkı yapmaktadır. Ayrıca, tüm yerleşkeler içinde değişik fakülte ve bölümlerin bünyesinde bulunan çok
                sayıda kantin ve kafeterya da öğrencilerin beslenme ihtiyaçlarını karşılayabilmesine imkan tanımaktadır.
                Bu kantinler sağlığa uygunluk açısından sürekli olarak denetlenmekte ve fiyatlar da üniversite yönetimi
                tarafından belirlenmektedir.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <p><strong>Olimpiyat Kafe</strong></p>
            <p align="justify">
                Kanuni Kampüsü Koru Tesisleri altında bulunan Olimpiyat Kafe kaliteli hizmet ve kusursuz servis anlayışı
                ile hizmet sunmaktadır.
            </p>
            <p>
                Telefon: 0 (462) 377 3404
            </p>
        </div>

        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_34b9d.jpg" class="img-polaroid" width="200"
                alt="Olimpiyat Kafe" data-toggle="modal" data-target="#myModal6">
            <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Olimpiyat Kafe</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/koru_44139.JPG"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <p><strong>Sahil Tesisleri</strong></p>
            <p align="justify">
                Kanuni Kampüsü Koru Tesisleri altında bulunan Olimpiyat Kafe kaliteli hizmet ve kusursuz servis anlayışı
                ile hizmet sunmaktadır.
            </p>
            <p>
                Telefon: 0 (462) 377 3404
            </p>
        </div>
        <div class="col-md-3">

            <img src="http://www.ktu.edu.tr/dosyalar/ktu_b803b.jpg" class="img-polaroid" width="200"
                alt="Sahil Tesisleri" data-toggle="modal" data-target="#myModal7">
            <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Sahil Tesisleri</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_49129.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p><strong>Kantinler</strong></p>
            <p align="justify">
            </p>
            <ul style="list-style: disc !important; padding-left:30px;">
                <li>İİBF Kantini</li>
                <li>İnşaat-Mimarlık Kantini</li>
                <li>Makine Kantini</li>
                <li>Matematik Kantini</li>
                <li>Orman Fakültesi Kantini</li>
                <li>Yer Bilimleri Kantini</li>
                <li>Doktor's Kafe (Tıp Fakültesi)</li>
                <li>Sevimli Kantini (Fizik Bölümü Altı)</li>
                <li>Çınaraltı Lokantası</li>
                <li>Kafe5</li>
            </ul>
            <p></p>
        </div>
        <div class="col-md-3"><br>

            <img src="http://www.ktu.edu.tr/dosyalar/01_00_00_42133.jpg" class="img-polaroid hoverZoomLink" width="200"
                alt="Makine Kantini" data-toggle="modal" data-target="#myModal8">
            <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Makine Kantini</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/01_00_00_42133.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3"><br>

            <img src="http://www.ktu.edu.tr/dosyalar/ktu_908a4.jpg" class="img-polaroid" width="200" alt="İİBF Kantini"
                data-toggle="modal" data-target="#myModal8">
            <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">İİBF Kantini</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/16_00_00_4f2fe.JPG"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div style="width:10px;height:15px;"></div>
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_1d948.jpg" class="img-polaroid" width="200"
                alt="Matematik Kantini" data-toggle="modal" data-target="#myModal9">
            <div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Matematik Kantini</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_787c2.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <div class="row" >
        <div class="col-md-12">
            <h3 class="page-banner" style="padding:5px;">SAĞLIK</h3><br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p align="justify">
                5510 sayılı Sosyal Güvenlik ve Genel Sağlık Sigortası kanununun 60. maddesinde belirtilen koşullar
                dahilinde tüm öğrenciler, Genel Sağlık Sigortası kapsamına alınmış olup tedavi giderleri Sosyal Güvenlik
                Kurumu (SGK) tarafından karşılanmaktadır.
            </p>
            <p>
            </p>
            <div style="border-bottom: 1px solid #ddd;background-color: #f9f9f9;">Sağlık, Kültür ve Spor Daire
                Başkanlığı</div>
            Telefon: 0 (462) 377 2005<br>
            Faks: 0 (462) 325 3161<br>
            Web Sayfası: <a href="https://www.ktu.edu.tr/sks" target="_blank">www.ktu.edu.tr/sks</a>
            <p></p>
        </div>
    </div>


</div>
              """),
            ),
            SingleChildScrollView(
              child: Html(data: """ 
             <div class="tab-pane " id="sosyalyasam2">
    <div class="row" >
        <div class="col-md-12">
            <p align="justify">
                Karadeniz Teknik Üniversitesi, sosyal açıdan ülkemizin en önde gelen üniversitelerinden biri olup
                öğrencilerin her türlü sosyal, sportif, kültürel amaçlı ihtiyaçlarını karşılayabileceği çok sayıda
                olanağa sahiptir.
            </p>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <h3 class="page-banner" style="padding:5px;">SOSYAL-KÜLTÜREL AMAÇLI TESİSLER</h3><br>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <p><b>Atatürk Kültür Merkezi</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-7">
            <p align="justify">
                Merkez Kanuni Yerleşkesi içerisinde bulunan Atatürk Kültür Merkezi, 1000 kişilik ve 250 kişilik oturma
                kapasiteli olmak üzere iki ana salona sahiptir. Ayrıca, bunlara ek hizmet sağlayan küçük salonlar da
                mevcuttur. Bu merkez, sinema, tiyatro, konser, konferans ve panel gibi her türlü bilimsel ve kültürel
                faaliyetin yürütülmesine imkan tanımaktadır.
            </p>
        </div>
        <div class="col-md-5">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_767ec.jpg" class="img-polaroid" width="200" hspace="10px;"
                alt="Atatürk Kültür Merkezi" data-toggle="modal" data-target="#myModal10">
            <div class="modal fade" id="myModal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Atatürk Kültür Merkezi</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/kongremerkezleri_e487c.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>

            <img src="http://www.ktu.edu.tr/dosyalar/ktu_2cec7.jpg" class="img-polaroid" width="200"
                alt="Atatürk Kültür Merkezi" data-toggle="modal" data-target="#myModal12">
            <div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Atatürk Kültür Merkezi</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/kongremerkezleri_db444.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-6">
            <p><b>Prof. Dr. Osman Turan Kültür ve Kongre Merkezi</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-7">
            <p align="justify">
                Toplam 8000 m2'lik bir alan üzerinde inşa edilen Kongre ve Kültür Merkezi, her türlü bilimsel ve
                kültürel faaliyetin yürütülebileceği modern bir donanıma sahiptir. İçerisinde 600 ve 250 kişilik oturma
                kapasiteli 2 büyük salon, 80 ve 120'şer kişilik toplam 6 seminer salonu, 250 ve 600 m2'lik 2 sergi
                salonu, 1 müze ve 220 kişilik 1 kokteyl salonu bulunan merkezimiz, modern tefrişatı, teçhizatı,
                simültane çeviriye uygunluğu, akustik ve ışık düzenlemesi ile her türlü ulusal ve uluslararası kongreye
                ev sahipliği yapacak özelliktedir. Kongre ve Kültür Merkezi'nde açık ve kapalı otopark ile kafeterya da
                bulunmaktadır.
            </p>
            <p>
                Telefon: 0 (462) 377 4025<br>
                Web Sayfası: <a href="http://www.ktu.edu.tr/kongremerkezleri"
                    target="_blank">http://www.ktu.edu.tr/kongremerkezleri</a>
            </p>
        </div>
        <div class="col-md-5">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_87a81.jpg" class="img-polaroid" width="200" hspace="10px;"
                alt="Prof. Dr. Osman Turan Kültür ve Kongre Merkezi" data-toggle="modal" data-target="#myModal11">
            <div class="modal fade" id="myModal11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Prof. Dr. Osman Turan Kültür ve Kongre Merkezi
                            </h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/kongremerkezleri_8631d.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>

            <img src="http://www.ktu.edu.tr/dosyalar/ktu_70158.jpg" class="img-polaroid" width="200"
                alt="Prof. Dr. Osman Turan Kültür ve Kongre Merkezi" data-toggle="modal" data-target="#myModal13">
            <div class="modal fade" id="myModal13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Prof. Dr. Osman Turan Kültür ve Kongre Merkezi
                            </h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/kongremerkezleri_80f60.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row" >
        <div class="col-md-12">
            <h3 class="page-banner" style="padding:5px;">SPORTİF AMAÇLI TESİSLER</h3><br>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-12">
            <p>KTÜ, öğrencilerimizin sportif gelişmelerine katkı sağlamak ve sosyal gereksinimlerini karşılamak amacıyla
                her türlü imkanı sunmaktadır.</p>
            <p>
            </p>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-12">
            <p><b>Hasan Polat Kapalı Spor Salonu</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-9">
            <p align="justify">
                Spor Salonu, 3000m2 alana kurulu olup her türlü kapalı alan spor müsabakalarının yapılmasına uygun bir
                donanıma sahiptir. Salonda 600 kişilik oturma kapasiteli bir trübin, soyunma odaları, basın-ses düzeni
                odası, dinlenme lobisi, 2 adet sauna ve değişik amaçlı kullanılabilecek bürolar bulunmaktadır.
            </p>
            <p>Telefon: 0 (462) 377 2430</p>
        </div>
        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_83030.jpg" class="img-polaroid" width="200"
                alt="Hasan Polat Kapalı Spor Salonu" data-toggle="modal" data-target="#myModal14">
            <div class="modal fade" id="myModal14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hasan Polat Kapalı Spor Salonu</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_8f070.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <p><b>Stadyum</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-9">
            <p align="justify">
                Stadyum, 100x60 m ebatlı bir çim sahaya sahip olup bir tarafından üstü kapalı ve koltuklu, diğer iki
                tarafından ise sadece koltuklu tribünlerle çevrilidir. Ayrıca, stadyum 3000 kişilik oturma kapasitesine
                sahip olup modern ışıklandırması ve skorborduyla gece müsabakalarının da yapılabileceği bir tarzda
                düzenlenmiştir.
            </p>
            <p>Telefon: 0 (462) 377 2430</p>
        </div>
        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_b5935.jpg" class="img-polaroid" width="200"
                alt="KTÜ Futbol Sahası" data-toggle="modal" data-target="#myModal15">
            <div class="modal fade" id="myModal15" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">KTÜ Futbol Sahası</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_5472a.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <p><b>Olimpik Yüzme Havuzu</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-9">
            <p align="justify">
                Turizm, Eğitim ve Uygulama Merkezi iktisadi İşletmesi bünyesinde yer alan olimpik yüzme havuzu, tüm su
                sporlarının yapılmasına uygundur. Aynı anda 250 kişinin faydalanabileceği havuzda, duş kabinli ve
                kilitlenebilir dolaplı soyunma odaları mevcut olup, havuz suyu son sistem filtrasyon ve otomatik
                ilaçlama sistemi ile temizlenmektedir. Ana havuzun bitişiğinde 5x5x0.6 m ebatlı bir çocuk havuzu da
                bulunmaktadır. Havuz kenarında güneşlenme imkanları ile beslenme gereksinimi için bir kafeterya ve
                sağlık hizmetleri için revir özelliği taşıyan bir sağlık kabini mevcuttur.
            </p>
            <p>Telefon: 0 (462) 377 4970 - 377 3679</p>
        </div>
        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_38085.jpg" class="img-polaroid" width="200"
                alt="KTÜ Olimpik Yüzme Havuzu" data-toggle="modal" data-target="#myModal16">
            <div class="modal fade" id="myModal16" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">KTÜ Olimpik Yüzme Havuzu</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_03ce9.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <p><b>Mini Sahalar</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-9">
            <p align="justify">
                Üniversitemizde, 3 minyatür futbol sahası, 6 tenis kortu, 2 açık basketbol sahası, 2 voleybol sahası ve
                1 squaş sahası bulunmaktadır. Bütün bu spor tesisleri granül kauçuk zeminli ve ışıklandırmalı olup, okul
                saatleri dışında da öğrencilerin spor yapmasına uygun imkanlar sunmaktadır.
            </p>
            <p>Telefon: 0 (462) 377 2430</p>
        </div>
        <div class="col-md-3">
            <img src="http://www.ktu.edu.tr/dosyalar/ktu_fc0e8.jpg" class="img-polaroid" width="200"
                alt="KTÜ Mini Sahalar" data-toggle="modal" data-target="#myModal17">
            <div class="modal fade" id="myModal17" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">KTÜ Mini Sahalar</h4>
                        </div>
                        <div class="modal-body"><img src="http://www.ktu.edu.tr/dosyalar/ktu_30eca.jpg"
                                class="img-polaroid" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <p><b>Sağlıklı Yaşam Tesisleri</b></p>
            <p>
            </p>
        </div>
    </div>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-12">
            <p align="justify">
                Çim saha kapalı trübini altında yeni oluşturulan spor kompleksi bünyesinde sağlıklı yaşam kapsamında
                kullanılabilecek sauna salonu, ağırlık merkezi, step salonu, düşünce sporları salonu ve diğer sağlıklı
                yaşam için spor faaliyetlerine uygun salonlar mevcuttur.
            </p>
            <p>Telefon: 0 (462) 377 2430</p>
        </div>
    </div>


</div>
              
              """),
            ),
          ],
        ),
      ),
    );
  }
}
