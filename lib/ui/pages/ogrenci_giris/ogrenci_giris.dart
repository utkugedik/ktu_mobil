import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:gap/gap.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/core/extensions/string_extensions.dart';
import 'package:ktu_mobil/core/routing/router.gr.dart';
import 'package:ktu_mobil/ui/shared/custom_button.dart';
import 'package:ktu_mobil/ui/shared/custom_textfield.dart';
import 'package:ktu_mobil/ui/shared/focus_escape.dart';

class OgrenciGirisPage extends HookConsumerWidget {
  const OgrenciGirisPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final rememberMe = useState(false);
    final studentNumberFocusNode = useFocusNode();
    final passwordFocusNode = useFocusNode();
    final studentNumberController = useTextEditingController();
    final passwordController = useTextEditingController();
    return FocusEscape(
      child: Scaffold(
        body: Form(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Align(
              child: Column(
                children: [
                  const Gap(40),
                  Text(
                    'Öğrenci Bilgi Paketi',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const Gap(80),
                  Row(
                    children: [
                      Expanded(
                        child: CustomTextField(
                          prefixIcon: const Icon(Icons.person),
                          textEditingController: studentNumberController,
                          label: 'Öğrenci Numarası',
                          textFieldType: TextFieldType.phone,
                          focusNode: studentNumberFocusNode,
                          nextFocusNode: passwordFocusNode,
                          validate: (v) => v!.hasData(fieldName: 'Öğrenci Numarası'),
                        ),
                      ),
                      Text(
                        '@ktu.edu.tr',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  CustomTextField(
                    prefixIcon: const Icon(Icons.lock),
                    textEditingController: passwordController,
                    textFieldType: TextFieldType.password,
                    label: 'Şifre',
                    focusNode: passwordFocusNode,
                    validate: (v) => v!.hasData(fieldName: 'Şifre'),
                  ),
                  Row(
                    children: [
                      const Text('Beni Hatırla'),
                      Switch(
                        value: rememberMe.value,
                        onChanged: (bool value) {
                          rememberMe.value = value;
                        },
                      )
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                      onPressed: () {},
                      child: const Text('Şifremi Unuttum'),
                    ),
                  ),
                  CustomButton(
                    buttonText: 'GİRİŞ YAP',
                    onPressed: () => context.navigateTo(const OgrenciAnasayfaRoute()),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
