import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/core/routing/router.gr.dart';
import 'package:ktu_mobil/ui/pages/akademik_birimler/constants/akademik_birimler_constants.dart';
import 'package:ktu_mobil/ui/shared/custom_appbar.dart';
import 'package:ktu_mobil/ui/shared/custom_divider.dart';
import 'package:ktu_mobil/ui/shared/styles/colors.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AkademikBirimlerPage extends HookConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: const CustomAppbar(title: 'birimler'),
      body: ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (_, __) => const CustomDivier(),
        itemCount: AkademikBirimlerConstants.unitList.length,
        itemBuilder: (_, index) {
          var list = AkademikBirimlerConstants.unitList[index];
          return ListTile(
            onTap: () => context.navigateTo(const AkademikBirimlerDetayRoute()),
            leading: Icon(
              MdiIcons.viewList,
              color: AppColors.blue800,
            ),
            title: Text(list.unitName),
            trailing: const Icon(
              Icons.navigate_next,
              size: 30,
            ),
          );
        },
      ),
    );
  }
}
