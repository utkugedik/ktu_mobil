class AkademikBirimlerModel {
  String unitName;
  bool hasChild;

  AkademikBirimlerModel({
    required this.unitName,
    required this.hasChild,
  });
}
