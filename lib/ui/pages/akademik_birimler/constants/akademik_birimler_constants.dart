import 'package:ktu_mobil/ui/pages/akademik_birimler/model/akademik_birimler_model.dart';

class AkademikBirimlerConstants {
  static final List<AkademikBirimlerModel> unitList = [
    AkademikBirimlerModel(
      unitName: 'FAKÜLTE',
      hasChild: false,
    ),
    AkademikBirimlerModel(
      unitName: 'YÜKSEKOKUL',
      hasChild: false,
    ),
    AkademikBirimlerModel(
      unitName: 'MESLEK YÜKSEKOKULU',
      hasChild: false,
    ),
    AkademikBirimlerModel(
      unitName: 'ENSTİTÜ',
      hasChild: false,
    ),
  ];
}
