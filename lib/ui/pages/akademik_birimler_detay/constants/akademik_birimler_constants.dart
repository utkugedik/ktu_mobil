import 'package:ktu_mobil/ui/pages/akademik_birimler_detay/model/akademik_birimler_detay_yonetim_model.dart';
import 'package:ktu_mobil/ui/shared/model/tabbar_model.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AkademikBirimlerDetayConstants {
  static const String genelBilgiHTML = """ 
 <div id="ortaktu"> 
                     <script>document.title ='İktisadi ve İdari Bilimler Fakültesi | Hakkımızda';</script>		 
                   <!-- Start Page Banner -->
                  <div class="page-banner" style="padding:35px 0;">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-6">
                          <h2 style="margin-top:15px; line-height:22px !important;">Hakkımızda</h2>
                        </div>
                        <div class="col-md-6">
                          <ul class="breadcrumbs">
                              <li style="list-style-type: none;"><a href="index.php">KTÜ</a></li>
                            <li style="list-style-type: none;"><a href="iibf">IIBF</a></li>
                            <li style="list-style-type: none;">Hakkımızda</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Page Banner -->
                   
                       <div id="content">
                    <div class="container">
                  <div class="page-content">
                      <div class="row">
                          <div class="col-md-12">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                <tbody>
                  <tr>
                    <td><img alt="" src="https://www.ktu.edu.tr/dosyalar/iibf_b6466.png" style="width: 100%;"></td>
                  </tr>
                  <tr>
                    <td>
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">İktisadi ve İdari Bilimler Fakültesi; Trabzon İktisadi ve Ticari İlimler Akademisi (TİTİA)’nin, 4 Kasım 1981 tarih ve 2547 sayılı Yükseköğretim Kanunu’nun kabul edilmesinden sonra, 20 Temmuz 1982 tarih ve 41 sayılı Kanun Hükmünde Kararname<a name="_ftnref1"></a><a href="file:///E:/di%C4%9Fer/1%20tarih%C3%A7e%20(2017).docx#_ftn1"><strong>[1]</strong></a>&nbsp;ile fakülteye dönüştürülüp Rektörlüğe bağlanması suretiyle kurulmuştur.</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">10 Aralık 1979 tarihinde İktisat ve İşletme olmak üzere iki bölüm ve 200 öğrenci ile Ortahisar İlçesi Boztepe Mahallesindeki Nemlizade Konağı olarak bilinen tarihi ahşap binada öğretime başlayan Trabzon İktisadi ve Ticari İlimler Akademisi, 1982 yılında fakülteye dönüştürülmesinin ardından, Kanuni Kampüsü’ne taşınarak Mühendislik Fakültesine bağlı Makine Mühendisliği Bölümü’nün zemin katında öğretime devam etmiştir. Yaklaşık 14 yıl süreyle Makine, İnşaat, Orman Endüstri, Fizik, Elektrik-Elektronik&nbsp; Mühendisliği Bölümlerinin dersliklerini kullanan Fakülte, 03 Ekim 1996 tarihinde iki bloktan oluşan ve&nbsp;<strong>13.560</strong>&nbsp;m<sup>2</sup>&nbsp;kapalı alanı olan yeni binasına taşınmıştır. 2011 yılında ise öğrenci bloğuna bitişik olarak inşa edilen ek binanın hizmete girmesi ile birlikte Fakültenin kapalı kullanım alanı&nbsp;<strong>17.845</strong>&nbsp;m<sup>2</sup>’ye ulaşmıştır.</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">1991 yılından itibaren hızla büyüyen Fakültede &nbsp;<strong>İktisat</strong>&nbsp;<strong>ve İşletme</strong>&nbsp;bölümlerinin yanında ayrıca&nbsp;<strong>Maliye</strong>,&nbsp;<strong>Uluslararası İlişkiler</strong>,&nbsp;<strong>Kamu Yönetimi</strong>,&nbsp;<strong>Çalışma Ekonomisi ve Endüstri İlişkileri</strong> ve&nbsp;<strong>Ekonometri</strong>&nbsp;bölümleri kurulmuştur. 1992 yılında kabul edilen 3843 sayılı Kanunla bu bölümlerin ikinci öğretim programları açılarak program&nbsp;sayısı 14’e ulaşmıştır. 2017 yılında&nbsp;<strong>Yönetim Bilişim Sistemleri Bölümü,&nbsp;</strong>2018 yılında ise&nbsp;<strong>%100 İngilizce</strong>&nbsp;eğitim veren<strong>&nbsp; Uluslararası İlişkiler </strong><strong>lisans&nbsp;</strong>programı açılmıştır. 2019 yılında&nbsp;ikinci öğretim programlarının kapatılmasından &nbsp;ardından &nbsp;Fakültemizde&nbsp;aktif olarak eğitime devam eden&nbsp;<strong>9&nbsp;lisans </strong><b>programı</b> bulunmaktadır.</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Uluslararası İlişkiler (%30 İngilizce) ve Kamu Yönetimi Bölümlerinde isteğe bağlı, %100 İngilizce Uluslararası İlişkiler lisans programında ise bir yıl zorunlu İngilizce hazırlık eğitimi bulunmaktadır. Fakültemizin&nbsp;bütün bölümlerde tezli yüksek lisans programı;&nbsp;İktisat, İşletme, Maliye, Uluslararası İlişkiler, Kamu Yönetimi ve Ekonometri Anabilim Dallarında ise doktora programı bulunmaktadır.</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">6 bini aşkın&nbsp;öğrenciye&nbsp;sahip olan Fakültede halen&nbsp;124&nbsp;akademik&nbsp;ve&nbsp;<strong>28</strong>&nbsp;idari personel görev yapmaktadır.&nbsp;</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Fakültede halen ikisi 168 ve biri 336 öğrenci kapasiteli olmak üzere 3 amfi, 1 seminer salonu, 40’ar öğrenci kapasiteli 2 bilgisayar laboratuvarı, kapasiteleri 60 ile 160 öğrenci arasında değişen&nbsp;28&nbsp;adet derslik ve&nbsp;149 adet&nbsp;personel çalışma ofisi&nbsp;bulunmaktadır.</span></span></p>
            
                    <p style="text-align: justify;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;<a href="file:///E:/di%C4%9Fer/1%20tarih%C3%A7e%20(2017).docx#_ftnref1" name="_ftn1" title=""><span calibri="">[1]</span></a> Yükseköğretim Kurumları Teşkilatı Hakkında 41 sayılı Kanun Hükmünde Kararname, 23.08.1983 tarih ve 2809 sayılı Kanunla Değiştirilerek Kanunlaşmıştır (30.03.1983 tarih ve 18003 sayılı Resmi Gazetede)</span></span></p>
                    </td>
                  </tr>
                  <tr>
                    <td><img alt="" src="https://www.ktu.edu.tr/dosyalar/iibf_b6466.png" style="width: 100%;"></td>
                  </tr>
                </tbody>
            </table>
            
            <h3 class="MsoNormal" style="color:#aaa;font-style:italic;"><br>
            .&nbsp;&nbsp;</h3>
                               </div>
                             </div>
                       <div class="row">
                          <div class="col-md-12">
                                                         </div>
                             </div>
                                <div class="row">
                          <div class="col-md-12">
                                                         </div>
                             </div>
                                <div class="row">
                          <div class="col-md-12">
                                                         </div>
                             </div>
                                <div class="row">
                            <div class="col-md-12">
                              <div class="post-bottom clearfix">
                                  <div class="post-tags-list">
                                    <a href="#"> Son Güncelleme : 16.03.2022</a> 
                                  </div>
                                  <!-- <div class="post-share">
                                    <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//www.ktu.edu.tr/2018/-" target="_blank"><i class="fab fa-facebook"></i></a>
                                    <a class="twitter" href="https://twitter.com/home?status=http%3A//www.ktu.edu.tr/2018/-" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//www.ktu.edu.tr/2018/-&title=&summary=&source=" target="_blank"><i class="fab fa-linkedin"></i></a>
                                  </div>-->
                              </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                
                           </div>
""";

  static final List<AkademikBirimlerDetayYonetimModel> yonetimListesi = [
    AkademikBirimlerDetayYonetimModel(
      name: 'Bölüm Baskant : DR. ÖGR. ÜYESi EKREM BAHÇEKAPILI',
      telephone: '04623771807',
      email: 'ekrem.bahcekapili@ktu.edu.tr',
    ),
    AkademikBirimlerDetayYonetimModel(
      name: 'Bölüm Baskan Yardimcisi : DR. ÖGR. ÜYESi MUSTAFA BiLGEHAN iMAMOGLU',
      telephone: '04623771800',
      email: 'bilgehan@ktu.edu.tr',
    ),
    AkademikBirimlerDetayYonetimModel(
      name: 'Bölüm Erasmus/AKTS koordinatörü : Doç. Dr. Muhammet BERIGEL',
      telephone: '04623771818',
      email: 'berigel@ktu.edu.tr',
    ),
    AkademikBirimlerDetayYonetimModel(
      name: 'Bölüm Sekreteri : Bilgisayar isletmeni Demet BAYRAKTAR',
      telephone: '04623772964',
      email: 'dbayraktar@ktu.edu.tr',
    ),
  ];

  static final List<TabbarModel> tabbarList = [
    TabbarModel(
      title: 'GENEL BİLGİ',
      icon: MdiIcons.information,
    ),
    TabbarModel(
      title: 'YÖNETİM',
      icon: MdiIcons.information,
    ),
    TabbarModel(
      title: 'DERSLER',
      icon: MdiIcons.information,
    ),
    TabbarModel(
      title: 'AKADEMİK PERSONEL',
      icon: MdiIcons.information,
    ),
  ];
}
