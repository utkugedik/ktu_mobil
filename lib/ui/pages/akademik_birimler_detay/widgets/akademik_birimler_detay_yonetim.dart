import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/ui/shared/custom_divider.dart';

class AkademikBirimlerDetayYonetim extends HookConsumerWidget {
  final String name, telephone, email;
  const AkademikBirimlerDetayYonetim({
    required this.name,
    required this.telephone,
    required this.email,
  });
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          color: Colors.blue[600],
          height: 40,
          width: double.infinity,
          child: Text(
            name,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Row(
          children: [
            Expanded(flex: 1, child: showStrings('Telefon')),
            Expanded(flex: 2, child: showStrings(telephone)),
          ],
        ),
        const CustomDivier(),
        Row(
          children: [
            Expanded(flex: 1, child: showStrings('E - Posta')),
            Expanded(flex: 2, child: showStrings(email)),
          ],
        ),
      ],
    );
  }

  Container showStrings(String name) {
    return Container(
      alignment: Alignment.centerLeft,
      height: 30,
      child: Text(
        name,
        style: const TextStyle(color: Colors.blueAccent),
      ),
    );
  }
}
