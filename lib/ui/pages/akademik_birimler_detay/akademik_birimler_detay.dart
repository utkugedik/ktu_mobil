import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/ui/pages/akademik_birimler_detay/constants/akademik_birimler_constants.dart';
import 'package:ktu_mobil/ui/pages/akademik_birimler_detay/widgets/akademik_birimler_detay_yonetim.dart';
import 'package:ktu_mobil/ui/shared/styles/colors.dart';

class AkademikBirimlerDetayPage extends HookConsumerWidget {
  const AkademikBirimlerDetayPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<String> list = ['Kodu', 'Ders adi', 'ECTS', 'D+U+L', 'Z/S', 'Dili', 'Grup'];
    return DefaultTabController(
      length: AkademikBirimlerDetayConstants.tabbarList.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('(I. ÖĞRETİM)'),
          bottom: TabBar(
            isScrollable: true,
            tabs: AkademikBirimlerDetayConstants.tabbarList
                .map(
                  (tab) => Tab(
                    text: tab.title,
                    icon: Icon(
                      tab.icon,
                      color: AppColors.black,
                    ),
                  ),
                )
                .toList(),
          ),
        ),
        body: TabBarView(
          children: [
            // genel bilgi
            SingleChildScrollView(
              child: SizedBox(
                width: 200,
                child: Html(data: AkademikBirimlerDetayConstants.genelBilgiHTML),
              ),
            ),

            // yonetim
            ListView.builder(
              itemCount: AkademikBirimlerDetayConstants.yonetimListesi.length,
              itemBuilder: (_, index) {
                var list = AkademikBirimlerDetayConstants.yonetimListesi[index];
                return AkademikBirimlerDetayYonetim(
                  name: list.name,
                  telephone: list.telephone,
                  email: list.email,
                );
              },
            ),

            // dersler
            Column(
              children: [
                Row(
                  children: list
                      .map(
                        (e) => Expanded(
                          child: Container(
                            height: 40,
                            decoration: BoxDecoration(
                              color: AppColors.blue800,
                              border: Border.symmetric(
                                vertical: BorderSide(width: 1, color: Colors.yellow[400]!),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                e,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        color: Colors.grey[400],
                        height: 25,
                        width: double.infinity,
                        child: const Center(
                          child: Text(
                            'GUZ DONEMI',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),

            // akademik personel
            ListView.builder(
              itemCount: AkademikBirimlerDetayConstants.yonetimListesi.length,
              itemBuilder: (_, index) {
                var list = AkademikBirimlerDetayConstants.yonetimListesi[index];
                return AkademikBirimlerDetayYonetim(
                  name: list.name,
                  telephone: list.telephone,
                  email: list.email,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
