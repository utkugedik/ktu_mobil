class AkademikBirimlerDetayYonetimModel {
  String name, telephone, email;

  AkademikBirimlerDetayYonetimModel({
    required this.name,
    required this.telephone,
    required this.email,
  });
}
