import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:ktu_mobil/core/extensions/list_extensions.dart';
import 'package:ktu_mobil/ui/pages/ana_sayfa/constants/ana_sayfa_constants.dart';
import 'package:ktu_mobil/ui/pages/ana_sayfa/model/ana_sayfa_model.dart';

class AnaSayfaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [...AnaSayfaConstants.anaSayfaFirstLineItems.map((e) => _buildFirstLaneItems(e, context)).toList()],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [...AnaSayfaConstants.anaSayfaSecondLineItems.map((e) => _buildFirstLaneItems(e, context)).toList()],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [...AnaSayfaConstants.anaSayfaThirdLineItems.map((e) => _buildFirstLaneItems(e, context)).toList()],
                ),
              ].withGap(50),
            ),
          ),
        ),
      ),
    );
  }
}

_buildFirstLaneItems(AnaSayfaModel model, BuildContext context) {
  return Column(
    children: [
      InkWell(
        onTap: (() => context.navigateTo(model.route)),
        child: Icon(
          model.icon,
          color: Colors.blue[800],
          size: 50,
        ),
      ),
      Text(
        model.title,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14,
        ),
      ),
    ],
  );
}
