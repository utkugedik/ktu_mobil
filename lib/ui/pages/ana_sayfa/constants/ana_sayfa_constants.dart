import 'package:flutter/material.dart';
import 'package:ktu_mobil/core/routing/router.gr.dart';
import 'package:ktu_mobil/ui/pages/ana_sayfa/model/ana_sayfa_model.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AnaSayfaConstants {
  static final List<AnaSayfaModel> anaSayfaFirstLineItems = [
    AnaSayfaModel(
      icon: MdiIcons.school,
      title: 'AKADEMİK \nBİRİMLER',
      route: const AkademikBirimlerRoute(),
    ),
    AnaSayfaModel(
      icon: MdiIcons.mapMarker,
      title: 'KTÜ \nHARİTA',
    ),
    AnaSayfaModel(
      icon: MdiIcons.calendar,
      title: 'AKADEMİK \nTAKVİM',
      route: const AkademikTakvimRoute(),
    ),
    AnaSayfaModel(
      icon: MdiIcons.phone,
      title: 'TELEFON \nREHBERİ',
      route: const TelefonRehberiRoute(),
    ),
  ];

  static final List<AnaSayfaModel> anaSayfaSecondLineItems = [
    AnaSayfaModel(
      icon: MdiIcons.messageAlert,
      title: 'DUYURU \nETKİNLİK',
    ),
    AnaSayfaModel(
      icon: Icons.subtitles,
      title: 'SOSYAL \nYAŞAM',
      route: const SosyalYasamRoute(),
    ),
    AnaSayfaModel(
      icon: MdiIcons.silverwareForkKnife,
      title: 'YEMEK \nLİSTESİ',
      route: const YemekListesiRoute(),
    ),
    AnaSayfaModel(
      icon: MdiIcons.email,
      title: 'KTÜ \nE POSTA',
    ),
  ];
  static final List<AnaSayfaModel> anaSayfaThirdLineItems = [
    AnaSayfaModel(
      icon: MdiIcons.clipboardAccount,
      title: 'ÖĞRENCİ \nGİRİŞİ',
      route: const OgrenciGirisRoute(),
    ),
    AnaSayfaModel(
      icon: MdiIcons.accountSupervisor,
      title: 'PERSONEL \nGİRİŞİ',
    ),
  ];
}
