import 'package:flutter/material.dart';

class AnaSayfaModel {
  IconData icon;
  String title;
  dynamic route;
  AnaSayfaModel({
    required this.icon,
    required this.title,
    this.route,
  });
}
