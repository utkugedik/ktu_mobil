import 'package:flutter/material.dart';

class OgrenciAnasayfaPanel extends StatelessWidget {
  final String title;
  final String info;

  const OgrenciAnasayfaPanel({required this.title, required this.info});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: SizedBox(
        height: 40,
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blue[400],
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Text(
                ': $info',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blue[900],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
