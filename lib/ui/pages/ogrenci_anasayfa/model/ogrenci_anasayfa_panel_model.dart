class OgrenciAnasayfaPanelModel {
  final String title, info;

  OgrenciAnasayfaPanelModel({
    required this.title,
    required this.info,
  });
}
