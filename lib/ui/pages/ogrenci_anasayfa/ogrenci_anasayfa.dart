import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/ui/pages/ogrenci_anasayfa/constants/ogrenci_anasayfa_constants.dart';
import 'package:ktu_mobil/ui/pages/ogrenci_anasayfa/widgets/ogrenci_anasayfa_panel.dart';

class OgrenciAnasayfaPage extends HookConsumerWidget {
  const OgrenciAnasayfaPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text('Utku Gedik'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.separated(
              itemCount: OgrenciAnasayfaConstants.liste.length,
              itemBuilder: (_, index) => OgrenciAnasayfaPanel(
                title: OgrenciAnasayfaConstants.liste[index].info,
                info: OgrenciAnasayfaConstants.liste[index].title,
              ),
              separatorBuilder: (_, __) => Divider(
                height: 10,
                color: Colors.blue[200],
              ),
            ),
          )
        ],
      ),
    );
  }
}
