import 'package:ktu_mobil/ui/pages/ogrenci_anasayfa/model/ogrenci_anasayfa_panel_model.dart';

class OgrenciAnasayfaConstants {
  static final List<OgrenciAnasayfaPanelModel> liste = [
    OgrenciAnasayfaPanelModel(
      info: 'ÖGRENCi NO',
      title: '385182',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'FAKÜLTE',
      title: 'iKTiSADi VE IDARi BiLiMLER FAKULTESI',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'BÖLÜM',
      title: 'YÖNETIM BiLiSiM SiSTEMLERi',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'PROGRAM',
      title: '( I. ÖGRETiM)',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'SINIF',
      title: '4. SINIF',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'EPOSTA',
      title: '385182@ogr.ktu.edu.tr',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'DIGER EPOSTA',
      title: 'info.utkugedik@gmail.com',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'DANISMAN',
      title: '',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'DANISMAN MAiL',
      title: '',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'ÖGRENiM DURUM',
      title: 'halen kayiti ögrencimizdir',
    ),
    OgrenciAnasayfaPanelModel(
      info: 'YÖKSiS DURUM',
      title: 'Aktif Ogrenci',
    ),
  ];
}
