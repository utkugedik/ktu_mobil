import 'package:ktu_mobil/ui/shared/model/tabbar_model.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class YemekListesiConstants {
  static final List<TabbarModel> liste = [
    TabbarModel(
      title: 'KAMPÜS YEMEKHANE',
      icon: MdiIcons.silverwareForkKnife,
    ),
    TabbarModel(
      title: 'SAHİL TESİSLERİ',
      icon: MdiIcons.silverwareForkKnife,
    ),
  ];
}
