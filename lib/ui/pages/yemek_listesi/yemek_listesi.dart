import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/ui/pages/yemek_listesi/constants/yemek_listesi_constants.dart';
import 'package:ktu_mobil/ui/shared/styles/colors.dart';

class YemekListesiPage extends HookConsumerWidget {
  const YemekListesiPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DefaultTabController(
      length: YemekListesiConstants.liste.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('YEMEK LİSTESİ'),
          bottom: TabBar(
            tabs: YemekListesiConstants.liste
                .map(
                  (tab) => Tab(
                    text: tab.title,
                    icon: Icon(tab.icon, color: tab.color),
                  ),
                )
                .toList(),
          ),
        ),
        body: TabBarView(
          children: [
            DataTable(
              showBottomBorder: true,
              columnSpacing: 1,
              dividerThickness: 1,
              headingTextStyle: Theme.of(context).textTheme.subtitle2,
              headingRowColor: MaterialStateColor.resolveWith((states) => AppColors.blue600),
              columns: const [
                DataColumn(
                  label: Text(
                    'Tarih Gun',
                  ),
                ),
                DataColumn(
                  label: Text('Yemek Menusu'),
                ),
                DataColumn(
                  label: Text('Kalori'),
                ),
              ],
              rows: const [
                DataRow(
                  cells: [
                    DataCell(
                      Text('1 Haziran 2022 Carsamba'),
                    ),
                    DataCell(
                      Text('Pırasa'),
                    ),
                    DataCell(
                      Text('150'),
                    ),
                  ],
                ),
                DataRow(
                  cells: [
                    DataCell(
                      Text('1 Haziran 2022 Carsamba'),
                    ),
                    DataCell(
                      Text('Nohut'),
                    ),
                    DataCell(
                      Text('100'),
                    ),
                  ],
                ),
              ],
            ),
            DataTable(
              showBottomBorder: true,
              columnSpacing: 1,
              dividerThickness: 1,
              headingTextStyle: Theme.of(context).textTheme.subtitle2,
              headingRowColor: MaterialStateColor.resolveWith((states) => AppColors.blue600),
              columns: const [
                DataColumn(
                  label: Text(
                    'Tarih Gun',
                  ),
                ),
                DataColumn(
                  label: Text('Yemek Menusu'),
                ),
                DataColumn(
                  label: Text('Kalori'),
                ),
              ],
              rows: const [
                DataRow(
                  cells: [
                    DataCell(
                      Text('1 Haziran 2022 Carsamba'),
                    ),
                    DataCell(
                      Text('Pilav'),
                    ),
                    DataCell(
                      Text('90'),
                    ),
                  ],
                ),
                DataRow(
                  cells: [
                    DataCell(
                      Text('1 Haziran 2022 Carsamba'),
                    ),
                    DataCell(
                      Text('Çorba'),
                    ),
                    DataCell(
                      Text('60'),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
