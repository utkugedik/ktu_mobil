import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:ktu_mobil/ui/shared/styles/colors.dart';

enum TextFieldType {
  normal,
  mail,
  password,
  phone,
}

class CustomTextField extends HookConsumerWidget {
  final TextEditingController textEditingController;
  final String? prefixText, label, value, headerText, hintText, helperText;
  final bool isParagraph;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final FocusNode? focusNode, nextFocusNode;
  final Function(String)? onChange;
  final Function(String)? onFiedlSubmitted;
  final FormFieldValidator<String>? validate;
  final int? minimumLine;
  final int? maximumLine;
  final int? maxLength;
  final TextFieldType? textFieldType;
  final bool? isAllCharactersUppercase;
  final TextStyle? placeHolderTextStyle;
  final bool displayBorder;
  final bool? isFilled;
  final Color? fillColor;
  final Color? focusedBorderColor;
  final Widget? suffixIcon, prefixIcon;
  final IconData? prefixIconData;
  final bool readOnly;
  final VoidCallback? onTap, onEditingComplete;
  final InputBorder? inputBorder;
  final TextStyle? textStyle, labelStyle;
  final EdgeInsetsGeometry? contentPadding;

  const CustomTextField(
      {required this.textEditingController,
      this.hintText,
      this.onChange,
      this.isFilled,
      this.fillColor,
      this.focusedBorderColor,
      this.onEditingComplete,
      this.isParagraph = false,
      this.textInputAction = TextInputAction.next,
      this.textInputType,
      this.focusNode,
      this.nextFocusNode,
      this.validate,
      this.value,
      this.prefixText,
      this.helperText,
      this.label,
      this.headerText,
      this.labelStyle,
      this.minimumLine = 1,
      this.maximumLine,
      this.maxLength,
      this.textFieldType = TextFieldType.normal,
      this.isAllCharactersUppercase = false,
      this.placeHolderTextStyle,
      this.displayBorder = true,
      this.readOnly = false,
      this.onTap,
      this.onFiedlSubmitted,
      this.suffixIcon,
      this.prefixIcon,
      this.prefixIconData,
      this.inputBorder,
      this.textStyle,
      this.contentPadding});

  TextInputType getTextInputType() {
    switch (textFieldType) {
      case TextFieldType.normal:
        return TextInputType.text;
      case TextFieldType.phone:
        return TextInputType.number;
      case TextFieldType.mail:
        return TextInputType.emailAddress;
      default:
        return TextInputType.text;
    }
  }

  _nextFocus(FocusNode? nextFocusNode, BuildContext context, FocusNode fieldFocusNode) {
    fieldFocusNode.unfocus();
    if (nextFocusNode != null) {
      FocusScope.of(context).requestFocus(nextFocusNode);
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final obscureIfPassword = useState(textFieldType == TextFieldType.password);
    final fieldFocusNode = focusNode ?? useFocusNode();

    InputBorder _getBorderStyle(
      bool displayBorder, {
      bool isFocused = false,
      bool hasError = false,
    }) =>
        displayBorder
            ? inputBorder == null
                ? UnderlineInputBorder(
                    borderRadius: BorderRadius.circular(6),
                    borderSide: BorderSide(
                      color: hasError
                          ? AppColors.punch
                          : isFocused || textEditingController.text.isNotEmpty
                              ? focusedBorderColor ?? AppColors.black
                              : AppColors.black,
                    ),
                  )
                : inputBorder!
            : InputBorder.none;

    Widget _buildEyeIcon() => IconButton(
          onPressed: () => obscureIfPassword.value = !obscureIfPassword.value,
          color: AppColors.black,
          iconSize: 18,
          icon: Icon(obscureIfPassword.value ? MaterialCommunityIcons.eye_outline : MaterialCommunityIcons.eye_off_outline),
        );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        headerText != null
            ? Padding(
                padding: const EdgeInsets.only(top: 24),
                child: Text(
                  headerText!,
                ),
              )
            : const SizedBox(),
        Padding(
          padding: EdgeInsets.symmetric(vertical: headerText != null ? 6 : 0),
          child: TextFormField(
            onTap: onTap,
            controller: textEditingController,
            focusNode: focusNode,
            readOnly: readOnly,
            style: textStyle ?? Theme.of(context).textTheme.bodyText1,
            cursorColor: AppColors.black,
            minLines: isParagraph ? minimumLine : null,
            maxLines: isParagraph ? maximumLine : 1,
            validator: validate,
            textInputAction: textInputAction,
            keyboardType: textInputType ?? getTextInputType(),
            maxLength: maxLength,
            textCapitalization: isAllCharactersUppercase! ? TextCapitalization.characters : TextCapitalization.none,
            obscureText: obscureIfPassword.value,
            decoration: InputDecoration(
              fillColor: fillColor,
              filled: isFilled,
              helperText: helperText,
              helperMaxLines: 3,
              helperStyle: Theme.of(context).textTheme.bodyText1,
              errorStyle: Theme.of(context).textTheme.bodyText2!.copyWith(color: AppColors.punch),
              contentPadding: contentPadding ?? const EdgeInsets.symmetric(horizontal: 8, vertical: 14),
              alignLabelWithHint: true,
              labelText: label,
              floatingLabelStyle: Theme.of(context).textTheme.bodyText1,
              labelStyle: labelStyle ?? Theme.of(context).textTheme.bodyText1!.copyWith(color: AppColors.grey3),
              prefixText: prefixText,
              suffixIcon: textFieldType == TextFieldType.password ? _buildEyeIcon() : suffixIcon,
              // suffixIconConstraints: const BoxConstraints(maxHeight: 1, maxWidth: 35),
              prefixIcon: prefixIcon ??
                  (prefixIconData != null
                      ? Icon(
                          prefixIconData,
                          size: 20,
                        )
                      : null),
              hintText: hintText,
              hintMaxLines: minimumLine,
              hintStyle: placeHolderTextStyle ?? Theme.of(context).textTheme.bodyText1!.copyWith(color: AppColors.grey3),
              counter: const Offstage(),
              border: _getBorderStyle(
                displayBorder,
              ),
              enabledBorder: _getBorderStyle(
                displayBorder,
              ),
              focusedBorder: _getBorderStyle(
                displayBorder,
                isFocused: true,
              ),
              errorBorder: _getBorderStyle(
                displayBorder,
                hasError: true,
              ),
            ),
            onChanged: onChange,
            onFieldSubmitted: onFiedlSubmitted ?? (_) => _nextFocus(nextFocusNode, context, fieldFocusNode),
            onEditingComplete: onEditingComplete,
          ),
        ),
      ],
    );
  }
}
