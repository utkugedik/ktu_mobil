import 'package:flutter/material.dart';

class TabbarModel {
  String title;
  IconData icon;
  Color color;
  TabbarModel({
    required this.title,
    required this.icon,
    this.color = Colors.black,
  });
}
