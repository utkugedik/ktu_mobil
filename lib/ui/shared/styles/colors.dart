import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Colors.white;
  static const Color black = Colors.black;
  static const Color blue = Colors.blue;
  static const Color grey = Colors.grey;
  static const Color grey3 = Color(0xff929292);
  static const Color green = Colors.green;
  static const Color amber = Colors.amber;
  static final Color yellow400 = Colors.yellow[400]!;
  static final Color blue100 = Colors.blue[100]!;
  static final Color blue600 = Colors.blue[600]!;
  static final Color blue800 = Colors.blue[800]!;
  static const Color punch = Color(0xffDC2D2D);
}
