import 'package:flutter/material.dart';
import 'package:ktu_mobil/ui/shared/styles/colors.dart';

class ThemeStyle {
  static ThemeData customThemeData() {
    // AppBar Theme
    AppBarTheme _getAppBarTheme() => AppBarTheme(
          elevation: 0.7,
          backgroundColor: Colors.blue[600],
          iconTheme: const IconThemeData(
            color: Colors.white,
          ),
        );

    // Text Theme
    TextTheme _getTextTheme() => const TextTheme(
          subtitle2: TextStyle(
            color: AppColors.white,
          ),
        );

    return ThemeData(
      primaryColor: Colors.blue[800],
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: _getAppBarTheme(),
      textTheme: _getTextTheme(),
    );
  }
}
