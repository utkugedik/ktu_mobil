import 'package:flutter/material.dart';

class CustomDivier extends StatelessWidget {
  final Color? color;
  const CustomDivier({this.color});

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Colors.blue[600],
      height: 0,
      thickness: 1,
    );
  }
}
