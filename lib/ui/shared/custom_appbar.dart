import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/core/routing/router.gr.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CustomAppbar extends HookConsumerWidget with PreferredSizeWidget {
  final String title;

  const CustomAppbar({required this.title});
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return useMemoized(
      () => AppBar(
        title: Text(title.toUpperCase()),
        leading: IconButton(
          onPressed: () => context.popRoute(),
          padding: const EdgeInsets.only(right: 10),
          constraints: const BoxConstraints(),
          icon: const Icon(Icons.arrow_back),
        ),
        actions: [
          IconButton(
            onPressed: () => context.router.pushAndPopUntil(const AnaSayfaRoute(), predicate: (_) => false),
            icon: const Icon(MdiIcons.home, size: 30),
          ),
        ],
      ),
      [title],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(52);
}
