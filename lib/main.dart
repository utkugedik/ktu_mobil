import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ktu_mobil/bootstrap.dart';
import 'package:ktu_mobil/core/providers/providers.dart';
import 'package:ktu_mobil/ui/shared/styles/theme.dart';

void main() async {
  await bootstrap(() => const MyApp());
}

class MyApp extends HookConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appRouter = ref.watch(routerProvider);

    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'KTü Mobil',
      theme: ThemeStyle.customThemeData(),
      routeInformationParser: appRouter.defaultRouteParser(),
      routerDelegate: appRouter.delegate(),
    );
  }
}
