import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'core/providers/providers.dart';
import 'core/routing/router.gr.dart';

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  WidgetsFlutterBinding.ensureInitialized();

  final appRouter = AppRouter();

  runZonedGuarded(
    () async => runApp(
      ProviderScope(
        overrides: [
          routerProvider.overrideWithValue(appRouter),
        ],
        child: await builder(),
      ),
    ),
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}
