import 'package:auto_route/auto_route.dart';

import 'package:ktu_mobil/ui/pages/akademik_birimler/akademik_birimler.dart';
import 'package:ktu_mobil/ui/pages/akademik_birimler_detay/akademik_birimler_detay.dart';
import 'package:ktu_mobil/ui/pages/akademik_takvim/akademik_takvim.dart';
import 'package:ktu_mobil/ui/pages/ana_sayfa/ana_sayfa.dart';
import 'package:ktu_mobil/ui/pages/ogrenci_anasayfa/ogrenci_anasayfa.dart';
import 'package:ktu_mobil/ui/pages/ogrenci_giris/ogrenci_giris.dart';
import 'package:ktu_mobil/ui/pages/sosyal_yasam/sosyal_yasam.dart';
import 'package:ktu_mobil/ui/pages/telefon_rehberi/telefon_rehberi.dart';
import 'package:ktu_mobil/ui/pages/yemek_listesi/yemek_listesi.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: AnaSayfaPage, initial: true),
    AutoRoute(page: AkademikBirimlerPage),
    AutoRoute(page: AkademikTakvimPage),
    AutoRoute(page: TelefonRehberiPage),
    AutoRoute(page: SosyalYasamPage),
    AutoRoute(page: YemekListesiPage),
    AutoRoute(page: OgrenciGirisPage),
    AutoRoute(page: AkademikBirimlerDetayPage),
    AutoRoute(page: OgrenciAnasayfaPage),
  ],
)
class $AppRouter {}
