// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i10;
import 'package:flutter/material.dart' as _i11;
import 'package:ktu_mobil/ui/pages/akademik_birimler/akademik_birimler.dart'
    as _i2;
import 'package:ktu_mobil/ui/pages/akademik_birimler_detay/akademik_birimler_detay.dart'
    as _i8;
import 'package:ktu_mobil/ui/pages/akademik_takvim/akademik_takvim.dart' as _i3;
import 'package:ktu_mobil/ui/pages/ana_sayfa/ana_sayfa.dart' as _i1;
import 'package:ktu_mobil/ui/pages/ogrenci_anasayfa/ogrenci_anasayfa.dart'
    as _i9;
import 'package:ktu_mobil/ui/pages/ogrenci_giris/ogrenci_giris.dart' as _i7;
import 'package:ktu_mobil/ui/pages/sosyal_yasam/sosyal_yasam.dart' as _i5;
import 'package:ktu_mobil/ui/pages/telefon_rehberi/telefon_rehberi.dart' as _i4;
import 'package:ktu_mobil/ui/pages/yemek_listesi/yemek_listesi.dart' as _i6;

class AppRouter extends _i10.RootStackRouter {
  AppRouter([_i11.GlobalKey<_i11.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i10.PageFactory> pagesMap = {
    AnaSayfaRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: _i1.AnaSayfaPage());
    },
    AkademikBirimlerRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: _i2.AkademikBirimlerPage());
    },
    AkademikTakvimRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i3.AkademikTakvimPage());
    },
    TelefonRehberiRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.TelefonRehberiPage());
    },
    SosyalYasamRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i5.SosyalYasamPage());
    },
    YemekListesiRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i6.YemekListesiPage());
    },
    OgrenciGirisRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i7.OgrenciGirisPage());
    },
    AkademikBirimlerDetayRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i8.AkademikBirimlerDetayPage());
    },
    OgrenciAnasayfaRoute.name: (routeData) {
      return _i10.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i9.OgrenciAnasayfaPage());
    }
  };

  @override
  List<_i10.RouteConfig> get routes => [
        _i10.RouteConfig(AnaSayfaRoute.name, path: '/'),
        _i10.RouteConfig(AkademikBirimlerRoute.name,
            path: '/akademik-birimler-page'),
        _i10.RouteConfig(AkademikTakvimRoute.name,
            path: '/akademik-takvim-page'),
        _i10.RouteConfig(TelefonRehberiRoute.name,
            path: '/telefon-rehberi-page'),
        _i10.RouteConfig(SosyalYasamRoute.name, path: '/sosyal-yasam-page'),
        _i10.RouteConfig(YemekListesiRoute.name, path: '/yemek-listesi-page'),
        _i10.RouteConfig(OgrenciGirisRoute.name, path: '/ogrenci-giris-page'),
        _i10.RouteConfig(AkademikBirimlerDetayRoute.name,
            path: '/akademik-birimler-detay-page'),
        _i10.RouteConfig(OgrenciAnasayfaRoute.name,
            path: '/ogrenci-anasayfa-page')
      ];
}

/// generated route for
/// [_i1.AnaSayfaPage]
class AnaSayfaRoute extends _i10.PageRouteInfo<void> {
  const AnaSayfaRoute() : super(AnaSayfaRoute.name, path: '/');

  static const String name = 'AnaSayfaRoute';
}

/// generated route for
/// [_i2.AkademikBirimlerPage]
class AkademikBirimlerRoute extends _i10.PageRouteInfo<void> {
  const AkademikBirimlerRoute()
      : super(AkademikBirimlerRoute.name, path: '/akademik-birimler-page');

  static const String name = 'AkademikBirimlerRoute';
}

/// generated route for
/// [_i3.AkademikTakvimPage]
class AkademikTakvimRoute extends _i10.PageRouteInfo<void> {
  const AkademikTakvimRoute()
      : super(AkademikTakvimRoute.name, path: '/akademik-takvim-page');

  static const String name = 'AkademikTakvimRoute';
}

/// generated route for
/// [_i4.TelefonRehberiPage]
class TelefonRehberiRoute extends _i10.PageRouteInfo<void> {
  const TelefonRehberiRoute()
      : super(TelefonRehberiRoute.name, path: '/telefon-rehberi-page');

  static const String name = 'TelefonRehberiRoute';
}

/// generated route for
/// [_i5.SosyalYasamPage]
class SosyalYasamRoute extends _i10.PageRouteInfo<void> {
  const SosyalYasamRoute()
      : super(SosyalYasamRoute.name, path: '/sosyal-yasam-page');

  static const String name = 'SosyalYasamRoute';
}

/// generated route for
/// [_i6.YemekListesiPage]
class YemekListesiRoute extends _i10.PageRouteInfo<void> {
  const YemekListesiRoute()
      : super(YemekListesiRoute.name, path: '/yemek-listesi-page');

  static const String name = 'YemekListesiRoute';
}

/// generated route for
/// [_i7.OgrenciGirisPage]
class OgrenciGirisRoute extends _i10.PageRouteInfo<void> {
  const OgrenciGirisRoute()
      : super(OgrenciGirisRoute.name, path: '/ogrenci-giris-page');

  static const String name = 'OgrenciGirisRoute';
}

/// generated route for
/// [_i8.AkademikBirimlerDetayPage]
class AkademikBirimlerDetayRoute extends _i10.PageRouteInfo<void> {
  const AkademikBirimlerDetayRoute()
      : super(AkademikBirimlerDetayRoute.name,
            path: '/akademik-birimler-detay-page');

  static const String name = 'AkademikBirimlerDetayRoute';
}

/// generated route for
/// [_i9.OgrenciAnasayfaPage]
class OgrenciAnasayfaRoute extends _i10.PageRouteInfo<void> {
  const OgrenciAnasayfaRoute()
      : super(OgrenciAnasayfaRoute.name, path: '/ogrenci-anasayfa-page');

  static const String name = 'OgrenciAnasayfaRoute';
}
