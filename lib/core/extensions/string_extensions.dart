import 'package:ktu_mobil/core/enums/regex_type.dart';

extension ValidationExtensions on String {
  String? hasMinLengthOf(int minLength) => length >= minLength ? null : "Bu alan en az $minLength karakter içermelidir";
  String? hasEqualLengthOf(int equalLength) => length == equalLength ? null : "Bu alan $equalLength karakter içermelidir";
  String? get isValidMail => RegexType.eMail.regex.hasMatch(this) ? null : "Geçerli bir mail adresi giriniz";

  String? hasExactLengthOf(int exactLength) => length == exactLength ? null : "Bu alan $exactLength karakterden oluşmalıdır";

  String? hasData({String? fieldName}) => isNotEmpty ? null : "$fieldName girilmesi zorunludur.";

  String? isInList(List<String> stringList, String? errorText) => stringList.contains(this) ? null : errorText ?? "Opsiyonlardan birini seçiniz";
  String? get isValidMobilePhone => RegexType.phoneNumber.regex.hasMatch(this) ? null : 'Lütfen geçerli bir cep telefonu numarası girin';
  String? get isValidPassword => RegexType.password.regex.hasMatch(this) ? null : "Lütfen geçerli bir şifre giriniz.";
  String? isConfirmPasswordMatch(String password) => compareTo(password) == 0 ? null : "Şifreler uyuşmuyor.";
}
